#include <QApplication>
#include "mainForm.h"

using namespace std;

int main(int argc, char *argv[]) {
    // initialize resources, if needed
    // Q_INIT_RESOURCE(resfile);

    QApplication app(argc, argv);

    // create and show your widgets here
    mainForm *MF = new mainForm();
    MF->showNormal();

    return app.exec();
}