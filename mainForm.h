#ifndef _MAINFORM_H
#define _MAINFORM_H

#include "ui_mainForm.h"

class mainForm : public QMainWindow {
    Q_OBJECT
private:
    std::array<QLineEdit*, 10> array_lineEdit;
    std::array<QLineEdit*, 10> array_delezi;
    std::array<QCheckBox*, 10> array_checkBoxi;
    Ui::mainForm widget;
public:
    mainForm();
    virtual ~mainForm();
private slots:
    void Pocisti();
    void Sestej();
    void IzracunajDeleze(short total_numbers, float divisor);
    void ZamenjajVejice();
    void SpremembaBoxa();
    void OProgramu();
    void Zapri();
    void IzberiZapri();
    void keyPressEvent(QKeyEvent *);
};

#endif /* _MAINFORM_H */
