#ifndef _ZAPRIFORM_H
#define _ZAPRIFORM_H

#include "ui_zapriForm.h"

class zapriForm : public QDialog {
    Q_OBJECT
public:
    zapriForm();
    virtual ~zapriForm();
private:
    Ui::zapriForm widget;
    bool zapri;
private slots:
    void PotrdiZapri();
};

#endif /* _ZAPRIFORM_H */
