#include "zapriForm.h"

zapriForm::zapriForm() {
    widget.setupUi(this);
    
    connect(widget.pushButton_OK, SIGNAL(clicked()), this, SLOT(PotrdiZapri()));
    
    //connect(widget.pushButton_brisi, SIGNAL(clicked()), this, SLOT(Pocisti()));
}

zapriForm::~zapriForm() {
}

void zapriForm::PotrdiZapri() {
    this->close();
}