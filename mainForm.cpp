#include "mainForm.h"
#include "zapriForm.h"
#include "QKeyEvent"

mainForm::mainForm() {
    widget.setupUi(this);
    // Array of all non-read only line edits.
    this->array_lineEdit = {{ 
        widget.lineEdit_1,
        widget.lineEdit_2,
        widget.lineEdit_3,
        widget.lineEdit_4,
        widget.lineEdit_5,
        widget.lineEdit_6,
        widget.lineEdit_7,
        widget.lineEdit_8,
        widget.lineEdit_9,
        widget.lineEdit_10
    }}; 
    // Array of ercentage line edits.
    this->array_delezi = {{
        widget.lineEdit_delez1,
        widget.lineEdit_delez2,
        widget.lineEdit_delez3,
        widget.lineEdit_delez4,
        widget.lineEdit_delez5,
        widget.lineEdit_delez6,
        widget.lineEdit_delez7,
        widget.lineEdit_delez8,
        widget.lineEdit_delez9,
        widget.lineEdit_delez10
    }};
    // Array of check boxes.
    this->array_checkBoxi = {{
        widget.checkBox_1,
        widget.checkBox_2,
        widget.checkBox_3,
        widget.checkBox_4,
        widget.checkBox_5,
        widget.checkBox_6,
        widget.checkBox_7,
        widget.checkBox_8,
        widget.checkBox_9,
        widget.checkBox_10,                
    }};
    QRegExp regExp("^[1-9]+\\d*([\\.|,]{0,1}\\d+){0,1}$");
    // Validate all line edits from array_lineEdit that they can only accept correct characters.
    for (int i = 0; i < 10; i++) {
        array_lineEdit[i]->setValidator(new QRegExpValidator(regExp, this));
    }    
    // Fill combo box with numbers from 2 to 10.
    for (int i = 2; i <= 10; i++) {
        widget.comboBox->addItem(QString::number(i));
    }    
    connect(widget.pushButton_brisi, SIGNAL(clicked()), this, SLOT(Pocisti()));
    connect(widget.pushButton_izracunaj, SIGNAL(clicked()), this, SLOT(Sestej()));
    connect(widget.comboBox, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(SpremembaBoxa(const QString&)));
    connect(widget.action_Zapri, SIGNAL(triggered()), this, SLOT(IzberiZapri()));
    connect(widget.action_O_programu, SIGNAL(triggered()), this, SLOT(IzberiZapri()));    
    for (int i = 0; i < 10; i++) {
        connect(array_lineEdit[i], SIGNAL(textChanged(const QString &)), this, SLOT(ZamenjajVejice()));
    }
    SpremembaBoxa();
}

mainForm::~mainForm() {
}

void mainForm::Pocisti() {
    // Clean all entry line edits.
    for (int i = 0; i < 10; i++) {
        this->array_lineEdit[i]->clear();
    }
    // Clean line edit for sum.
    widget.lineEdit_vsota->clear();
}

void mainForm::Sestej() {    
    // Check for potential changes.
    SpremembaBoxa();
    float divisor = 0;
    bool ok;
    // Number selected in combo box.
    int total_numbers = widget.comboBox->currentText().toInt(&ok);
    // Create float array with length of total_numbers.
    float * numbers = new float[total_numbers];    
    for (int i = 0; i < total_numbers; i++) {
        // If line edit is not disabled.
        if (this->array_lineEdit[i]->isEnabled() == true) {
            numbers[i] = (1 / this->array_lineEdit[i]->text().toFloat(&ok));
            divisor += (1 / this->array_lineEdit[i]->text().toFloat(&ok));
        }
        else {
            numbers[i] = 0;
            divisor = 0;
            break;
        }
    }    
    // Prints expected profit.
    widget.lineEdit_vsota->setText(QString("%1").arg(1/divisor));
    IzracunajDeleze(total_numbers, divisor);
}

void mainForm::IzracunajDeleze(short total_numbers, float divisor) {
    if (1/divisor <= 0) {
        for (int i = 0; i < total_numbers; i++) {
            this->array_delezi[i]->setText("0");
        }
        return;
    }
    float coefficient = 1 / divisor;
    // Contains info for each active check box if it is checked or not.
    bool checked_boxes [total_numbers];
    // Number of active checked boxes.
    short checked_number = 0;
    // Fill checked_boxes and count checked_number.
    for (int i = 0; i < total_numbers; i++) {
        if (this->array_checkBoxi[i]->isChecked() ) {
            checked_boxes[i] = true;
            checked_number++;
        }
        else {
            checked_boxes[i] = false;
        }
    }
    bool ok;    
    // If all enabled boxes are either checked or unchecked.
    if ((checked_number == total_numbers) | (checked_number == 0)) {
        for (int i = 0; i < total_numbers; i++) {
        float kvota = this->array_lineEdit[i]->text().toFloat(&ok);
        float value = std::floor((coefficient / kvota * 10000) + 0.5) / 100;
        this->array_delezi[i]->setText(QString("%1").arg(value));
        }
    }
    else {
        float divisor_for_unchecked = 0;
        // Sum of parts from enabld unchecked check boxes.
        float sum_of_non_checked_parts = 0;
        for (int i = 0; i < total_numbers; i++) {
            if (checked_boxes[i] == false) {
                float kvota = this->array_lineEdit[i]->text().toFloat(&ok);
                float value = std::floor((1 / kvota * 10000) + 0.5) / 100;
                this->array_delezi[i]->setText(QString("%1").arg(value));
                sum_of_non_checked_parts += value;
            }
            else {
                float kvota = this->array_lineEdit[i]->text().toFloat(&ok);
                divisor_for_unchecked += (1 / kvota);
            }
        }
        float sum_of_checked_parts = 1 - ( sum_of_non_checked_parts / 100 );
        float coefficient_of_unchecked = 1 / divisor_for_unchecked;
        for (int i = 0; i < total_numbers; i++) {
            if (checked_boxes[i] == true) {
                float kvota = this->array_lineEdit[i]->text().toFloat(&ok);
                //
                float value = std::floor( coefficient_of_unchecked / kvota * sum_of_checked_parts * 10000 + 0.5 ) / 100;
                this->array_delezi[i]->setText(QString("%1").arg(value));
            }
        }
    }
}

void mainForm::ZamenjajVejice() {
    QString a;
    for (int i = 0; i < 10; i++) {
        a = this->array_lineEdit[i]->text();
        a.replace(",",".");
        this->array_lineEdit[i]->setText(a);
    }
};

void mainForm::SpremembaBoxa() {
    bool ok;
    int box_num = widget.comboBox->currentText().toInt(&ok);
    
    for (int i = 0; i < 10; i++) {
        if ( (i+1) <= box_num) {
            this->array_lineEdit[i]->setEnabled(true);
            this->array_delezi[i]->setEnabled(true);
            this->array_checkBoxi[i]->setEnabled(true);
        }
        else {
            this->array_lineEdit[i]->setEnabled(false);
            this->array_delezi[i]->setEnabled(false);
            this->array_checkBoxi[i]->setEnabled(false);
        }   
    }
}

void mainForm::OProgramu() {
    
}

void mainForm::IzberiZapri() {
    zapriForm * ZF = new zapriForm();
    Qt::WindowFlags flags = ZF->windowFlags();
    ZF->setWindowFlags(flags | Qt::CustomizeWindowHint | Qt::WindowStaysOnTopHint | Qt::WindowMinimizeButtonHint);
    ZF->show();
}

void mainForm::Zapri() {
    this->close();
}

//
void mainForm::keyPressEvent(QKeyEvent *event) {
    // Calculates data
    if ((event->key() == Qt::Key_Return) || (event->key() == Qt::Key_Enter)) {
        Sestej();
    }
     if ((event->key() == Qt::Key_2)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(0);
        }
    }
     if ((event->key() == Qt::Key_3)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(1);
        }
    }
     if ((event->key() == Qt::Key_4)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(2);
        }
    }
    if ((event->key() == Qt::Key_5)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(3);
        }
    }
    if ((event->key() == Qt::Key_6)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(4);
        }
    }
    if ((event->key() == Qt::Key_7)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(5);
        }
    }
    if ((event->key() == Qt::Key_8)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(6);
        }
    }
    if ((event->key() == Qt::Key_9)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(7);
        }
    }
    if ((event->key() == Qt::Key_0)) {
        if (event->modifiers() == Qt::ShiftModifier) {
            widget.comboBox->setCurrentIndex(8);
        }
    }
}